// Скрипт для постсборки продакшн версии
var fs = require('fs');
var path = require('path');

// Удаляем index.html
fs.unlink(path.join('dist', 'index.html'), function(err) {
    if (err) {
        console.log("Can't remove index.html");
        console.log(err);
    }
});

// Изменяем manifest.json
fs.readFile(path.join('src', 'manifest.json'), 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    var lines = data.split('\n');
    var result = [];
    var cs = false;
    var csj = false;
    for (var i = 0, max = lines.length; i < max; ++i) {
        var line = lines[i];
        if (/"content_scripts":/.test(line)) { // Начинается наш блок
            cs = true;
        }
        if (cs && /"js":/.test(line)) { // А это уже то место, которое надо заменить
            csj = true;
        }
        if (!csj) {
            result.push(line)
        }
        if (csj && /]/.test(line)) { // Делаем замену и выходим из блока
            result.push('            "css": [');
            result.push('                "styles.bundle.css"');
            result.push('            ],');
            result.push('            "js": [');
            result.push('                "inline.bundle.js",');
            result.push('                "polyfills.bundle.js",');
            result.push('                "main.bundle.js"');
            result.push('            ]');
            cs = csj = false;
        }
    }
    fs.writeFile(path.join('dist', 'manifest.json'), result.join('\n'), 'utf8', function (err) {
        if (err) {
            return console.log(err);
        }
    });
});

