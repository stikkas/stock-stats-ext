/* Page JS activities */
$(document).ready(function () {

    // Load Theme
    getTheme(function(theme){
        if(theme)
        {
            $(".ss__box").removeClass("ss__box--dark");
            $(".ss__box").removeClass("ss__box--white");
            $(".ss__box").addClass('ss__box--' + theme);

            if(theme == 'white')
            {
                $('.ss__dark').removeClass('active');
                $('.ss__dark').addClass('clickable');
            }
        }
    });

    $(".ss__select .ss__open-dropdown").click(function (e) {
        e.preventDefault();
        $(this).siblings(".ss__dropdown").first().slideToggle();
    });

    $(".ss__dropdown-link").click(function (e) {
        e.preventDefault();
        var val = $(this).text();
        var dropdown = $(this).parents(".ss__dropdown");
        dropdown.slideUp();
        dropdown.siblings('.ss__open-dropdown').text(val);
        show_hide_sellers();
        update_sum();
    });

    $('body').on("click", ".ss__select-color .clickable", function(e){
        e.preventDefault();
        $(".ss__box").toggleClass("ss__box--dark ss__box--white");
        $(".ss__select-color__links").toggleClass('clickable');

        var ui_theme = $(".ss__box").hasClass('ss__box--white')?'white':'dark';
        setTheme(ui_theme);
    });
});

var buy_box = '';
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if(request.type == 'product_info') {
            var url = "http://www.amazon.com/dp/" + request.data.asin ;
            $('.ss_product-title a').text(request.data.product_name);
            $('.ss_product-title a').attr( 'href' , url);
            $('#asin').text(request.data.asin);
            $('#best_seller_rank').text(request.data.best_seller);
            $('.ss__keepa-link').attr('href', "https://keepa.com/#!product/1-"+request.data.asin);
            $('.ss__product-image img').attr('src', request.data.image);

            /*
            $("#product_title").html("<a href='"+url+"' target='_blank'>"+request.data.product_name+"</a> | ASIN: "+request.data.asin+" | Best Sellers Rank: "+request.data.best_seller);
            $("#keepa_link").attr('href', "https://keepa.com/#!product/1-"+request.data.asin);
            buy_box = request.data.buy_box;

            $(".ss__table tr").each(function(){
                if($(this).text().indexOf(buy_box) > -1)
                {
                    console.log("buy box found");
                    $(this).addClass('buy_box');
                }
            });
            */
        } else if(request.type == 'seller_info') {

            var seller = request.seller;

            var isBuyBox = (seller.name==buy_box)?" class='buy_box' ":"";
            var fba_color = seller.fba? 'ss__color-green' : 'ss__color-red';

            var stars = parseInt(seller.rating);
            var stars_percent = stars/5*100;

            seller.rating = seller.rating.split('stars')[1] || "";

            var html = `<div class="ss__table-line">
                    <div class="ss__col ss__col-1">
                        <a href="#" class="ss__visible ss__visible--hidden"></a>
                    </div>
                    <div class="ss__col ss__col-2">
                        <p><a href='` + seller.url + `' target='_blank'> `+ seller.name +`</a></p>
                    </div>
                    <div class="ss__col ss__col-3">
                        <p><span class="ss__valute">$</span> `+ seller.price +`</p>
                    </div>
                    <div class="ss__col ss__col-4">
                        <p class='stock' id="seller_`+ seller.id +`">Loading</p>
                    </div>
                    <div class="ss__col ss__col-5 fba">
                        <p class="`+ fba_color +`">`+ (seller.fba?'Yes':'No') +`</p>
                    </div>
                    <div class="ss__col ss__col-6">
                        <div class="ss__stars">
                            <div class="ss__stars-item" style="width: `+stars_percent+`%;"></div>
                        </div>
                        <span class="ss__stars-text">`+ seller.rating+`</span>
                    </div>
                </div>`;

            /*
            var html = "<tr "+ isBuyBox +">";

            html += "<td><input type='checkbox' /></td>";
            html += "<td>"+ seller.name +"</td>";
            html += "<td>"+ seller.price +"</td>";
            html += "<td id='seller_"+ seller.id +"'>Loading... </td>";
            html += "<td>"+ seller.fba +"</td>";
            html += "<td>"+ seller.rating +"</td>";

            html += "</tr>";
            */

            $(".ss__table").append(html);
            show_hide_sellers();

        }
        else if(request.type == 'stock_value') {
            $('#seller_' + request.seller_id).text(request.data);
            update_sum();
        }
    }
);

$(document).ready(function(){

    // get settings
    chrome.storage.sync.get({
        default_sellers: '0'
    }, function(items) {
        $("#seller_type").val(items.default_sellers);
    });

    var spl = window.location.href.split('#');
    if(spl[1])
    {
        var parts = spl[1].split('|');
        if(parts.length > 1)
        {
            var asin = parts[0];
            var country = parts[1];
            chrome.runtime.sendMessage({action: "GetSellers", asin: asin, country: country});
        }
    }

    // Hide Selected Rows
    $('body').on("click",".ss__visible--hidden",function(ev){
        ev.preventDefault();
        $(this).parents('.ss__table-line').hide();
        update_sum();
    });
    $("#show_rows").click(show_rows);
    $("#seller_type").change(function(){

    });

});


/*
 * Shows or Hides sellers based on select menu value
 * @returns {undefined}
 */
function show_hide_sellers() {
    switch($(".ss__open-dropdown").text()) {
        case 'MF & FBA Sellers':
            show_rows();
            break;
        case 'Only MF Sellers':
            show_sellers(false);
            break;
        case 'Only FBA Sellers':
            show_sellers(true);
            break;
    }
}


/*
 * ---------------------------------------------------------------
 *  Hide Selected Rows
 * ---------------------------------------------------------------
 */
function hide_rows() {
    var table = $(".ss__table");
    table.find(".ss__table-line").each(function(index){
        if($(this).find("input").is(":checked"))
            $(this).hide();
    });
    update_sum();
}


/*
 * ---------------------------------------------------------------
 *  Show All Rows
 * ---------------------------------------------------------------
 */
function show_rows() {
    var table = $(".ss__table");
    table.find(".ss__table-line").each(function(index){
        $(this).show();
    });
    update_sum();
}


/*
 * ---------------------------------------------------------------
 *  Show FBA Seller only
 * ---------------------------------------------------------------
 */
function show_sellers(fba) {
    var table = $(".ss__table");
    table.find(".ss__table-line").each(function(index){
        var condition;
        if(fba)
            condition = 'No';
        else
            condition = 'Yes';

        if($(this).find('.fba').text().trim() === condition)
        {
            $(this).hide();
        }
        else
            $(this).show();
    });
}


/*
 * ---------------------------------------------------------------
 *  Calculate total and average stocks
 * ---------------------------------------------------------------
 */
function calc_stock() {
    var sum = 0;
    var count = 0;
    var table = $(".ss__table");
    table.find(".ss__table-line:visible").each(function(index){
        var stock = parseFloat($(this).find('.stock').text().trim()) || 0;
        if(!isNaN(stock))
        {
            sum += stock;
            count++;
        }
    });

    var avg = sum/count || 0;
    avg = Math.round(avg,2);
    return {sum:sum, avg:avg};
}

function update_sum() {
    var res = calc_stock();
    $("#total_stock").text(res.sum);
    $("#avg_stock").text(res.avg);
}
