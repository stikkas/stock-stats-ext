/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

async function testServerKeys() {
    for(var i = 71; i < 106; i++)
    {
        // var url_get = "http://localhost/keyserver/test/index.php?id=" + i;
        var url_get = "https://zally.io/KeyServer2/keys_v2/test/index.php?id=" + i;


        console.log('Getting key: ' + i);
        $.get(url_get, function(data){
            if(data && data.hasOwnProperty('status') && data.status === 'success')
            {
                var public = data.data.public;
                var private = data.data.private;
                var assoc = data.data.associate;
                var country = data.data.country;

                if(country == 'jp')
                    country = 'co.jp';

                if(country == 'uk')
                    country = 'co.uk';

                if(country == 'us')
                    country = 'com';

                var amz_url = searchQuery(public, private, assoc, "pen", country);

                $.get(amz_url, function(response){
                    if(response.indexOf('AWS.InvalidAssociate') > -1)
                    {
                        keyBad(i);
                    }
                    else
                    {
                        console.log("Key okay");
                    }

                }, 'text').fail(function(xhr){
                    var data = xhr.responseText;
                    if(data.indexOf('AWS.InvalidAssociate') > -1)
                    {
                        keyBad(i);
                    }
                    else
                    {
                        console.log("Key okay");
                    }
                });

            }
            else
            {
                console.error("Key " + i + " getting failed!");
            }

        }, "json");

        await sleep(2000);
    }

    function keyBad(i)
    {
        console.error("Key rejected: " + i);

        var url_invalid = "https://zally.io/KeyServer2/keys_v2/test/invalid.php?id=" + i;

        $.get(url_invalid, function(d){
            if(d && d.hasOwnProperty('status') && d.status === 'success')
            {
                console.log("successfully invalidated");
            }
        });
    }

    function searchQuery(public, private, assoc, keywords, country)
    {
        if(!country)
            country = 'com';

        var parameters = [];
        parameters.push("AWSAccessKeyId=" + public);
        parameters.push("AssociateTag=" + assoc);

        parameters.push("Operation=ItemSearch");
        parameters.push("Keywords=notebook");
        parameters.push("SearchIndex=All");
        parameters.push("ResponseGroup=ItemAttributes");
        //parameters.push("Service=AWSECommerceService");
        parameters.push("Timestamp=" + encodeURIComponent(timestamp()));
        //parameters.push("Version=2011-08-01");
        parameters.sort();
        var paramString = parameters.join('&');
        var signingKey = "GET\n" + "webservices.amazon."+country+"\n" + "/onca/xml\n" + paramString

        var signature = sha256(signingKey, private);
        signature = encodeURIComponent(signature);
        var amazonUrl = "http://webservices.amazon."+country+"/onca/xml?" + paramString + "&Signature=" + signature;
        return amazonUrl;
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}

