import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
    enableProdMode();
}

let shown;
let initialized;

const toggleApp = function (show: boolean) {
    document.getElementsByTagName('app-root').item(0)
            .setAttribute('style', `display:${show ? 'block' : 'none'};`);
}

window['chrome'].runtime.onMessage.addListener((request) => {
            if (request.message === 'clicked_browser_action') {
                if (!initialized) {
                    document.body.appendChild(document.createElement('app-root'));
                    platformBrowserDynamic().bootstrapModule(AppModule)
                            .then(() => shown = initialized = true)
                            .catch(err => console.log(err));
                } else {
                    shown = !shown;
                    toggleApp(shown);
                }
            }
        }
);

