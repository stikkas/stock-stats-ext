import {Component, NgZone, OnInit} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

    private runtime = window['chrome'].runtime;

    private asinRegExp = /\.?amazon\.(.+?)\/.+?\/dp\/(\w+?)[/?]/;

    /**
     * Интересующий продукт
     */
    product = <any>{};

    /**
     * Продавцы, торгующие данным продуктом
     */
    sellers = <any[]>[];

    /**
     * Флаг указывающий продавцов какого типа отображать
     */
    sellerType: string;

    constructor(private ngZone: NgZone) {
    }

    ngOnInit(): void {
        this.runtime.onMessage.addListener((request) => {
            switch (request.type) {
                case 'PRODUCT':
                    this.ngZone.run(() => this.product = request.data);
                    break;
                case 'SELLER':
                    this.sellerInfoHandler(request.data);
                    break;
                case 'STOCK':
                    this.stockInfoHandler(request.data);
                    break;
            }
        });
        this.getInfo();
    }

    private getInfo() {
        const found = this.asinRegExp.exec(window.location.href);
        if (found) {
            this.runtime.sendMessage({action: 'PRODUCT', asin: found[2], domain: found[1]});
        }
    }

    private sellerInfoHandler(seller: any) {

    }

    private stockInfoHandler(stock: any) {

    }

    /**
     * Возвращает список продавцов для отображения
     */
    getSellers(): any[] {
        if (this.sellerType) {
            return this.sellers.filter(seller => seller.type === this.sellerType);
        }
        return this.sellers;
    }

    /**
     * Возвращает строку с ответом да или нет в зависимости является ли продавец FBA
     * @param {String} type - тип продавца
     */
    isFBA(type: string): string {
        return type === 'FBA' ? 'yes' : 'no';
    }

    /**
     * Нужна для того чтобы ангуляр не перерисовывал все строки
     */
    trackByFn(index: number, item: any): number {
        return index; // best item.id
    }
}


