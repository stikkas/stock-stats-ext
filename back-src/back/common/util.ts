const CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
const DICT = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

export class Util {
    static uuid(len = 0, radix = 0) {
        const chars = CHARS;
        const uuid = [];
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (let i = 0; i < len; i++) {
                uuid[i] = chars[0 | Math.random() * radix];
            }
        } else {
            // rfc4122, version 4 form
            let r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (let i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | Math.random() * 16;
                    uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }

        return uuid.join('');
    }

    static randomCode(length: number): string {
        return Util.array(length).map(i => DICT[Util.rand(0, DICT.length - 1)]).join('');
    }

    static array(size: number): number[] {
        const arr = [];
        for (let i = 0; i < size; ++i) {
            arr.push(i);
        }
        return arr;
    }

    static rand(from: number, to: number): number {
        return Math.round(Math.random() * (to - from) + from);
    }

    static getAmazonHost(url: string): string {
        return url.match(/https?:\/\/(www|smile)\.amazon\.[\w.]+/)[0];
    }
}

