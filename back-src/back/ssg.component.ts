import {Component, OnDestroy, OnInit} from '@angular/core';
import {AmazonClientService} from './services/amazon.client.service';

@Component({
    selector: 'ssg-root',
    template: '<span></span>'
})
export class SsgComponent implements OnInit, OnDestroy {
    private chrome: any = window['chrome'];

    /**
     * Привязка this к обработчику clickButtonListener
     */
    private clickButtonWrapper: () => void;

    /**
     * Привязка this к обработчику onMessageListener
     */
    private messageWrapper: (request, sender, callback) => void;

    constructor(private amzClient: AmazonClientService) {
    }

    ngOnInit() {
        const self = this;
        this.clickButtonWrapper = function () {
            self.clickButtonListener();
        };

        this.messageWrapper = function (request, sender) {
            self.onMessageListener(request, sender);
            return true;
        };

        this.chrome.browserAction.onClicked.addListener(this.clickButtonWrapper);
        this.chrome.runtime.onMessage.addListener(this.messageWrapper);
    }

    ngOnDestroy() {
        this.chrome.browserAction.onClicked.removeListener(this.clickButtonWrapper);
        this.chrome.browserAction.onMessage.removeListener(this.messageWrapper);
    }

    /**
     * Обработчик события клацинья по кнопке в менюбаре
     */
    private clickButtonListener() {
        this.chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
            this.chrome.tabs.sendMessage(tabs[0].id, {'message': 'clicked_browser_action'});
        });
    }

    /**
     * Общение со странице контента
     */
    private onMessageListener({action, asin, domain}, sender) {
        if (action === 'PRODUCT') {
            this.amzClient.info(asin, sender.tab.id, domain);
        }
    }
}


