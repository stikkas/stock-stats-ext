import {Injectable} from '@angular/core';

/**
 * Сервис служит для получения JSON данных из строки с HTML
 */
@Injectable()
export class ParserService {

    /**
     * textContent возвращает строку с текстом всех потомков
     * данная функция возвращает только собственный текст элемента, без потомков
     * @param {Element} el - html элемент, чей текст нужен
     * @returns {string} - текст заданного элемента
     */
    private ownText(el: Element): string {
        return Array.from(el.childNodes).filter(it => it.nodeType === 3)
                .map(it => it.textContent).reduce((acc, it) => `${acc} ${it}`);
    }

    /**
     * Заменяет все найденные паттрены в заданой строке на необходимую строку
     * @param {string} input  - входная строка
     * @param {string} what  - паттерн который нужно заменить
     * @param {string} than  - паттерн на который нужно заменить
     * @returns {string} - строка с новым паттерном
     */
    private replaceAll(input: string, what: string, than: string): string {
        return input['replace'](new RegExp(what, 'g'), than);
    }

    /**
     * Возвращает textContent элемента, если найден элемент, или пустую строку в противном случае
     * @param {Element} el - отправная точка поиска
     * @param {string} selector - что ищем
     * @param {string} defaultValue - значение по умолчанию, если ничего не найдено
     * @param {string} attrName - если задан, то значение этого атрибута элемента
     * @returns {string} - результат
     */
    private text(el: Element, selector: string, defaultValue: string = '', attrName: string = 'textContent'): string {
        const found = el.querySelector(selector);
        return found ? found[attrName] : defaultValue;
    }

    /**
     * Возвращает данные по товару
     * @param {string} html - строка html страницы о товаре
     * @returns {any} - объект с интересующими нас данными
     */
    getProductInfo(html: string): any {
        const product = {
            title: '',
            image: '',
            sellerName: '',
            bsr: ''
        };
        if (/id=['"]productTitle['"]/.test(html)) { // Парсим если только есть название, иначе возвращаем пустой объект
            const body = new DOMParser().parseFromString(html, 'text/html').body;
            product.title = body.querySelector('#productTitle').textContent;
            product.sellerName = this.text(body, '#merchant-info a', 'Amazon.com');
            product.image = this.text(body, '#landingImage', '', 'src');
            if (product.image.startsWith('data:')) {
                product.image = body.querySelector('#landingImage').getAttribute('data-old-hires');
            }

            const salesRank = body.querySelector('#SalesRank');
            if (salesRank) {
                product.bsr = this.ownText(salesRank);
            } else {
                const bulletsContent = body.querySelector('#detail-bullets .content');
                if (bulletsContent) {
                    Array.from(bulletsContent.querySelectorAll('li')).some(li => {
                        const keyNode = li.querySelector('b');
                        if (keyNode) {
                            let key = keyNode.textContent.trim();
                            key = key.substr(0, key.length - 1);
                            if (key === 'Amazon Best Sellers Rank') {
                                product.bsr = this.ownText(li);
                                return true;
                            }
                        }
                    });
                }
            }
        }
        return product;
    }

    /**
     * Оригинальная версия, может пригодится
     * @param {string} html
     * @returns {any}
     */
    parseProductData(html: string): any {
        if (!html) {
            return 'Unable to Extract Data';
        }
        /*
        [['onclick', 'href'], ['onerror', 'fuck'], ['<script', '<!--'], ['</script>', '-->'], ['<img', '<dpd']]
                .forEach(it => html = this.replaceAll(html, it[0], it[1]));
        */

        if (!/id=('|")productTitle('|")/.test(html)) {
            return 'Unknown Data Format. Extraction Failed';
        }
        const body = new DOMParser().parseFromString(html, 'text/html').body;

        const ourData = {
            title: body.querySelector('#productTitle').textContent,
            sellerName: '',
            image: '',
            rank: '',
            category: '',
            reviewCount: '',
            reviewRating: '',
            noOfSellers: '',
            sellersLink: '',
            itemWeight: '',
            productDimensions: '',
            bestSellersRank: ''
        };
        ourData.sellerName = this.text(body, '#merchant-info a', 'Amazon.com');

        const detailsSection1 = body.querySelectorAll('#productDetails_detailBullets_sections1');
        if (detailsSection1.length > 0) {
            detailsSection1.forEach(it => {
                it.querySelectorAll('tr').forEach(tr => {
                    const children = tr.children;
                    if (children.length > 0) {
                        const key = children[0].textContent.trim();
                        const value = children[0].textContent;
                        ourData[key] = value;
                    }
                });
            });

            ourData.reviewCount = this.text(body, '#acrCustomerReviewText');
            ourData.reviewRating = this.text(body, '.reviewCountTextLinkedHistogram', '', 'title');

            const feature = body.querySelector('#olp_feature_div a');
            if (feature) {
                ourData.noOfSellers = feature.textContent;
                ourData.sellersLink = `http://www.amazon.com${feature['href']}`;
            }
        } else {
            let detailBullets = body.querySelector('#detail-bullets');
            if (detailBullets) {
                const bulletsContent = detailBullets.querySelector('.content');
                if (bulletsContent) {
                    bulletsContent.querySelectorAll('li').forEach(li => {
                        const keyNode = li.querySelector('b');
                        if (keyNode) {
                            let key = keyNode.textContent.trim();
                            key = key.substr(0, key.length - 1);
                            ourData[key] = this.ownText(li);
                        }
                    });
                }

                if (!!ourData['Amazon Best Sellers Rank']) {
                    ourData.bestSellersRank = ourData['Amazon Best Sellers Rank'];
                }

                if (!!ourData['Shipping Weight']) {
                    ourData.itemWeight = ourData['Shipping Weight'];
                }

                ourData.reviewCount = this.text(body, '#acrCustomerReviewText');
                ourData.reviewRating = this.text(body, '.reviewCountTextLinkedHistogram', '', 'title');

                const mbc = body.querySelector('#mbc a');
                if (mbc) {
                    ourData.noOfSellers = mbc.textContent;
                    ourData.sellersLink = `http://www.amazon.com${mbc['href']}`;
                }
            } else {
                detailBullets = body.querySelector('#detailBullets');
                if (detailBullets) {
                    const featureDiv = detailBullets.querySelector('#detailBullets_feature_div');
                    if (featureDiv) {
                        featureDiv.querySelectorAll('.a-list-item').forEach(it => {
                            const keyNode = it.querySelector('span');
                            if (keyNode) {
                                let key = keyNode.textContent.trim();
                                key = key.substr(0, key['length'] - 1);
                                const allSpans = it.querySelectorAll('span');
                                ourData[key] = allSpans.item(allSpans.length - 1).textContent;
                            }
                        });
                    }
                    const salesRank = body.querySelector('#SalesRank');
                    if (salesRank) {
                        ourData.bestSellersRank = this.ownText(salesRank);
                    }

                    if (!!ourData['Shipping Weight']) {
                        ourData.itemWeight = ourData['Shipping Weight'];
                    }
                    ourData.reviewCount = this.text(body, '#acrCustomerReviewText');
                    ourData.reviewRating = this.text(body, '.reviewCountTextLinkedHistogram', '', 'title');

                    const mbc = body.querySelector('#mbc a');
                    if (mbc) {
                        ourData.noOfSellers = mbc.textContent;
                        ourData.sellersLink = `http://www.amazon.com${mbc['href']}`;
                    }

                } else {
                    body.querySelectorAll('.techD table tr').forEach(tr => {
                        const tds = tr.children;
                        if (tds.length > 1) {
                            ourData[tds[0].textContent.trim()] = tds[1].textContent.trim();
                        }
                    });

                    ourData.reviewCount = this.text(body, '#averageCustomerReviewCount');
                    ourData.reviewRating = this.text(body, '#averageCustomerReviewRating');

                    const mbc = body.querySelector('#mbc a');
                    if (mbc) {
                        ourData.noOfSellers = mbc.textContent;
                        ourData.sellersLink = `http://www.amazon.com${mbc['href']}`;
                    }
                }
            }
        }

        if (!!ourData['review_count']) {
            ourData['review_count'] = ourData['review_count'].split(' ')[0];
        }

        if (!!ourData['review_rating']) {
            ourData['review_rating'] = ourData['review_rating'].split(' ')[0];
        }
        ourData['amzSells'] = this.text(body, '#merchant-info').indexOf('sold by Amazon.com') > -1 ? 'yes' : 'no';
        ourData['price'] = this.text(body, '#priceblock_ourprice', 'N/A');
        const rankInfo = ourData.bestSellersRank.split(' ')[0];
        const rankParts = rankInfo.split('(');
        if (rankParts.length > 0) {
            const diapason = rankParts[0].split(' in ');
            ourData['Best Sellers Rank'] = rankParts[0];
            if (diapason.length > 1) {
                ourData['rank'] = diapason[0].trim();
                ourData['category'] = diapason[1].trim();
            }
        }
        ourData['image'] = this.text(body, '#landingImage', '', 'src');
        return ourData;

    }

}

