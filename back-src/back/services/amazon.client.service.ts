import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ParserService} from './parser.service';

@Injectable()
export class AmazonClientService {
    constructor(private parser: ParserService,
                private http: HttpClient) {
    }

    info(asin: string, tabId: string, domain: string): void {
        this.http.get(`http://www.amazon.${domain}/gp/product/${asin}`, {responseType: 'text'})
                .subscribe(res => {
                            this.sendData({
                                type: 'PRODUCT',
                                data: Object.assign({asin: asin}, this.parser.getProductInfo(res))
                            }, tabId);
                        },
                        err => {
                            console.log(err);
                        }
                );

    }

    private sendData(data: any, tabId: string) {
        window['chrome'].tabs.sendMessage(tabId, data, () => {
        });
    }

}

