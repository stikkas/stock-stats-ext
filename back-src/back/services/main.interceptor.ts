import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UtilService} from './util.service';

const CLIENT_ID_HEADER = 'X-Client-ID';

@Injectable()
export class MainInterceptor /*implements HttpInterceptor*/ {
    // private clients = {};
    // private requests = {};
    // private context = {};
    //
    // constructor(private util: UtilService ) {}
    //
    // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     let myReq = req.clone({
    //         headers: {}
    //     });
    //
    //         let headers = req.headers;
    //         const clientId = headers.get(CLIENT_ID_HEADER);
    //         let result;
    //         if (clientId) {
    //             const host = this.util.getAmazonHost(req.url);
    //             // const requestId = d.requestId;
    //             const client = clients[clientId];
    //             const context = client.context[host];
    //             const cookies = Object.assign(this.util.cookies(headers.get('Cookie')), c => c.startsWith('x-amz')), context.cookies);
    //             context.userAgent = context.userAgent || USER_AGENT
    //                     .replace('$1', `${this.util.rand(501, 537)}`)
    //                     .replace('$2', `${this.util.rand(1, 37)}`)
    //                     .replace('$3', `${this.util.rand(58, 62)}`)
    //                     .replace('$4', `${this.util.rand(0, 7000)}`);
    //             requests[requestId] = clientId;
    //             headers = header(headers, 'User-Agent', context.userAgent);
    //             headers = header(headers, 'Cookie', this.util.objectToString(cookies, (k, v) => k + '=' + v, '; '));
    //             result = {requestHeaders: headers};
    //         }
    //         return result;
    //
    //     return next.handle(req);
    // }

}
