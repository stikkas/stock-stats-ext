import {Md5} from 'ts-md5';
import {Injectable} from '@angular/core';

@Injectable()
export class UtilService {

    private CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

    serialize(obj) {
        if (!obj) {
            return '';
        } else if (typeof obj === 'string') {
            return encodeURIComponent(obj);
        } else if (typeof obj === 'object') {
            let s = '';
            for (const k in obj) {
                if (obj.hasOwnProperty(k) && obj[k] !== undefined) {
                    if (s) {
                        s += '&';
                    }
                    s += encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]);
                }
            }
            return s;
        } else {
            throw new Error('Unsupported object type');
        }
    }

    isNotEmptyArray(arr) {
        return Array.isArray(arr) && !!arr.length;
    }

    map(arr, mapper) {
        if (!arr || !arr.length) {
            return {};
        }
        const res = {};
        arr.forEach(i => res[typeof mapper === 'string' ? i[mapper] : mapper(i)] = i);
        return res;
    }

    formatDate(ts) {
        const date = ts ? new Date(ts) : new Date();
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return month + '/' + day + '/' + year;
    }

    deleteParams = function (url) {
        const i = url.indexOf('?');
        if (i > 0) {
            url = url.substring(0, i);
        }
        return url;
    };

    left(s, sep) {
        if (!s || !sep) {
            return s;
        }
        const i = s.indexOf(sep);
        return i >= 0 ? s.substring(0, i) : s;
    }

    containsOne(where, ...what) {
        if (!where || !what || what.length === 0) {
            return false;
        }
        for (const s of what) {
            if (where.indexOf(s) >= 0) {
                return true;
            }
        }
        return false;
    }

    containsAll(where, ...what) {
        if (!where || !what || what.length === 0) {
            return false;
        }
        for (const s of what) {
            if (where.indexOf(s) < 0) {
                return false;
            }
        }
        return true;
    }

    getParameter(name, url) {
        const results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
        return results ? results[1] : null;
    }

    hasParam(url, params) {
        for (const i in params) {
            if (this.getParameter(params[i], url)) {
                return true;
            }
        }
        return false;
    }

    array(size) {
        const arr = [];
        for (let i = 0; i < size; ++i) {
            arr.push(i);
        }
        return arr;
    }

    objectToArray(obj, mapper) {
        return !obj || !mapper ? [] : Object.getOwnPropertyNames(obj).map(n => mapper(n, obj[n]));
    }

    objectToString(obj, mapper, separator) {
        return this.objectToArray(obj, mapper).join(separator);
    }

    arrayToObject(arr, keyMapper, valueMapper) {
        if (!keyMapper || !arr || !arr.length) {
            return {};
        }
        if (!valueMapper) {
            valueMapper = v => v;
        }
        const r = {};
        arr.forEach(i => {
            const key = keyMapper(i);
            const val = valueMapper(i);
            r[key] = val;
        });
        return r;
    }

    repeat(fn, timeout, times, callnow = true) {
        const self = this;
        return times <= 0 ? Promise.reject('times negative') : new Promise((resolve, reject) => {
            function schedule() {
                setTimeout(() => self.repeat(fn, timeout, times - 1).then(resolve, reject), timeout);
            }

            if (callnow) {
                try {
                    fn().then(resolve, schedule);
                } catch (e) {
                    reject(e);
                }
            } else {
                schedule();
            }
        });
    }

    md5(msg: string) {
        return Md5.hashStr(msg);
    }

    updateParameter(uri, key, value) {
        const re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
        const separator = uri.indexOf('?') !== -1 ? '&' : '?';
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + '=' + value + '$2');
        } else {
            return uri + separator + key + '=' + value;
        }
    }

    escapeHTML(s) {
        return s ? s.replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;') : '';
    }

    safe(fn, defaultVal?) {
        let val;
        try {
            val = fn();
        } catch (e) {
            val = defaultVal;
        }
        return val;
    }

    toNumber(s) {
        if (!s) {
            return Number.NaN;
        }
        let decimalChar;
        let thousandsChar;
        for (let i = 0; i < s.length; ++i) {
            const c = s[s.length - i - 1];
            if (isNaN(parseInt(c))) {
                if (i >= 3) {
                    thousandsChar = c;
                    break;
                } else {
                    decimalChar = c;
                }
            }
        }
        if (thousandsChar) {
            s = s.replace(new RegExp('\\' + thousandsChar, 'g'), '');
        }
        if (decimalChar) {
            s = s.replace(decimalChar, '.');
        }
        return Number(s);
    }

    unique(arr, field) {
        if (!arr || !arr.length || !field) {
            return arr;
        }
        const map = new Map();
        for (let i = 0; i < arr.length; ++i) {
            const p = arr[i];
            if (p) {
                const k = field ? p[field] : p;
                if (!map.has(k)) {
                    map.set(k, p);
                }
            }
        }
        return Array.from(map.values());
    }

    parseUrlParams(uri) {
        if (!uri) {
            return null;
        }
        const result = {};
        const idx = uri.indexOf('?');
        (idx >= 0 ? uri.substring(idx + 1) : uri).split('&').forEach(part => {
            const item = part.split('=');
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    }

    sum(obj) {
        return !obj ? null : typeof obj === 'number' ? obj : Object.getOwnPropertyNames(obj)
                .map(p => obj[p])
                .filter(v => !isNaN(v))
                .map(n => Number(n))
                .reduce((n1, n2) => n1 + n2, null);
    }

    clone(obj) {
        if (null == obj || 'object' !== typeof obj) {
            return obj;
        }
        const copy = obj.constructor();
        for (const attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = obj[attr];
            }
        }
        return copy;
    }

    minmax(min, max, val) {
        return this.min(this.max(min, val), max);
    }

    min(...args) {
        return this.safe(() => Array.prototype.slice.call(args)
                .filter(a => a !== undefined && a !== null).reduce((a1, a2) => a1 < a2 ? a1 : a2));
    }

    max(...args) {
        return this.safe(() => Array.prototype.slice.call(args)
                .filter(a => a !== undefined && a !== null).reduce((a1, a2) => a1 > a2 ? a1 : a2));
    }


    toCamelCase = function (s) {
        let state = 0;
        let r = '';
        for (let i = 0; i < s.length; ++i) {
            const c = s[i];
            switch (state) {
                case 0:
                    if (this.isLatinLetter(c) || this.isDigit(c)) {
                        r += c;
                    } else {
                        state = 1;
                    }
                    break;
                case 1:
                    if (this.isLatinLetter(c) || this.isDigit(c)) {
                        r += c.toUpperCase();
                        state = 0;
                    }
                    break;
            }
        }
        return r;
    }

    rand(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }

    replace(s, obj) {
        Object.getOwnPropertyNames(obj).forEach(n => s = s.replace(n, obj[n]));
        return s;
    }

    cookies(cookie, filter?) {
        const arr = (cookie || '').split(';').map(s => s.trim());
        return this.arrayToObject(filter ? arr.filter(filter) : arr, s => s.substring(0, s.indexOf('=')),
                s => s.substring(s.indexOf('=') + 1));
    }

    cookie(name) {
        return this.cookies(document.cookie)[name];
    }

    randomCode(length) {
        const DICT = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return this.array(length).map(i => DICT[this.rand(0, DICT.length - 1)]).join('');
    }

    uuid(len?, radix?) {
        const chars = this.CHARS;
        const uuid = [];
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (let i = 0; i < len; i++) {
                uuid[i] = chars[0 | Math.random() * radix];
            }
        } else {
            // rfc4122, version 4 form
            let r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (let i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | Math.random() * 16;
                    uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }

        return uuid.join('');
    }

    isLatinLetter(c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    isDigit(c) {
        return c >= '0' && c <= '9';
    }

    filter(obj, fltr: (obj: any, key: any) => boolean = this.nonEmpty) {
        return Array.isArray(obj) ? obj.filter(fltr) :
                this.arrayToObject(Object.getOwnPropertyNames(obj).filter(n => fltr(obj[n], n)), n => n, n => obj[n]);
    }

    empty(obj) {
        return obj === undefined || obj === null || (obj.length !== undefined && obj.length === 0) ||
                (typeof obj === 'object' && Object.getOwnPropertyNames(obj).length === 0);
    }

    nonEmpty(obj) {
        return !this.empty(obj);
    }

    money(n) {
        return isNaN(n) ? undefined : Math.ceil(n * 100 - 0.001) / 100;
    }

    isAmazonCategoryUrl(url) {
        return /https?:\/\/(www|smile)\.amazon\.[\w.]+\/([\w\d\-%]+\/)?b[/?]/.test(url);
    }

    isAmazonAuthorUrl(url) {
        return this.isAmazonUrl(url) && /\/e\/((:?B[A-Z\d]{9})|(:?\d{9}[A-Z\d]))($|\/|\?)/.test(url);
    }

    isAmazonDealsUrl(url) {
        return /https?:\/\/(www|smile)\.amazon\.[\w.]+\/sales\-deals\-[\w\d\-%]+($|\/|\?)/i.test(url);
    }

    isAmazonGoldBoxUrl(url) {
        return /https?:\/\/(www|smile)\.amazon\.[\w.]+\/gp\/goldbox($|\/|\?)/i.test(url);
    }

    isAmazonSearchUrl(url) {
        return this.isAmazonUrl(url) && (/\/l\/[\d]{8,16}($|\/|\?)/.test(url) || /\/s\//.test(url) || /\/search\//.test(url));
    }

    replaceAmazonImages(html) {
        return html.replace(/src=("https:\/\/images[\w\d\-.]+amazon.[\w.]+\/images\/[\w\d+\-%&.,_/]+")/gi, '_src=$1');
    }

    isAmazonAbsoluteUrl(url) {
        return /https?:\/\/(www|smile)\.amazon\.[\w.]+/.test(url);
    }

    getAmazonHost(url) {
        return url.match(/https?:\/\/(www|smile)\.amazon\.[\w.]+/)[0];
    }

    isAmazonUrl(url) {
        return /^https?:\/\/(www|smile)\.amazon\.[\w.]{2,6}\//.test(url.trim());
    }

    isAmazonProductUrl(url) {
        return this.isAmazonUrl(url) && !/\/stores\/node\//.test(url) && /(\/((B[A-Z\d]{9})|(\d{9}[A-Z\d]))($|\/|\?))|(\/dp\/((B[A-Z\d]{9})|(\d{9}[A-Z\d])))/.test(url);
    }

    getASIN(url) {
        return this.safe(() => url.match(/\/((:?B[A-Z\d]{9})|(:?\d{9}[A-Z\d]))($|\/|\?)/)[1]);
    }

    isAmazonBestSellersUrl(url) {
        return this.isAmazonUrl(url) && /\/(best\-sellers|bestsellers)/i.test(url);
    }

    getProductUrl(p) {
        return `https://www.amazon.${p.domain.toLowerCase().replace('_', '.')}/dp/${p.asin}`;
    }

    isAsin(s) {
        return /^(B[A-Z\d]{9})|(\d{9}[A-Z\d])/.test(s);
    }

    getAmazonDomainEnum(url) {
        return url.match(/amazon\.([\w.]+)/)[1].replace('.', '_').toUpperCase();
    }

    getAmazonHostByEnum(en) {
        return 'https://www.amazon.' + en.toLowerCase().replace('_', '.');
    }

    parse(html) {
        return new DOMParser().parseFromString(html, 'text/html');
    }

    eval(scr, ctx) {
        return (new Function('(function() {' + scr + '}).call(this)')).call(ctx);
    }
}
