import {Injectable} from '@angular/core';
import {UtilService} from './util.service';
import {HttpClient} from '@angular/common/http';

const MIME_JSON = 'application/json';
const MIME_FORM = 'application/x-www-form-urlencoded';
const HEADER_CONTENT_TYPE = 'Content-Type';


@Injectable()
export class AjaxService {
    private SERIALIZERS;

    constructor(private util: UtilService, private http: HttpClient) {
        this.SERIALIZERS = {
            'application/json': obj => JSON.stringify(obj),
            'application/x-www-form-urlencoded': obj => util.serialize(obj)
        };
    }

    ajax(url, method = 'GET', data, headers = {}) {
        if (typeof data === 'object') {
            if (method === 'GET') {
                const query = this.SERIALIZERS[MIME_FORM](data);
                url += (url.indexOf('?') === -1 ? '?' : '&') + query;
                data = null;
            } else {
                const contentType = headers[HEADER_CONTENT_TYPE] = headers[HEADER_CONTENT_TYPE] || MIME_JSON;
                const serializer = this.SERIALIZERS[contentType];
                data = serializer(data);
            }
        }
        return new Promise((resolve, reject) => {
            this.http.request(method, url, {
                body: data && typeof data === 'object' ? JSON.stringify(data) : data,
                headers: headers || {'Content-Type': MIME_JSON},
                withCredentials: true
            }).toPromise().then((response: any) => {
                if (response.ok) {
                    const contentType = response.headers.get(HEADER_CONTENT_TYPE);
                    if (contentType && contentType.indexOf(MIME_JSON) !== -1) {
                        response.json().then(resolve);
                    } else {
                        response.text().then(text => resolve(text.startsWith('{') && text.endsWith('}') ? JSON.parse(text) : text));
                    }
                } else {
                    reject(response.status);
                }
            }, () => {
                // console.log(arguments);
                reject('NETWORK');
            }).catch(e => {
                console.log(e);
                reject(e);
            });
        });
    }
}

