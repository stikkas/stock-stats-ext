import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {SsgComponent} from './ssg.component';
import {AmazonClientService} from './services/amazon.client.service';
import {AjaxService} from './services/ajax.service';
import {UtilService} from './services/util.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ParserService} from './services/parser.service';

// import {MainInterceptor} from './services/main.interceptor';

@NgModule({
    declarations: [
        SsgComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule
    ],
    providers: [
        AmazonClientService,
        AjaxService,
        ParserService,
        UtilService/*,
        [
            {
                provide: HTTP_INTERCEPTORS,
                useClass: MainInterceptor,
                multi: true
            }
        ]
        */
    ],
    bootstrap: [SsgComponent]
})
export class SsgModule {
}

